## Architecture overview

Program consists of three main parts \ref Fisy::Observers "Observers", \ref Fisy::Pipeline "Pipeline", \ref Fisy::Transfers "Transfers" (same as folder structure) and some other files/classes to parse input and construct these.

![schema](dataFlow.png)


Both Observers and Transfers have similar layout:
classes \ref Fisy::Observers::Observe "Observe" and \ref Fisy::Transfers::Transfer "Transfer" lists multiple Observers/Transfers for each \ref Fisy::MediaType via their respective interfaces and both interfaces have abstract base class that implements them.
Observers are, on top of this, split to three levels based on what are observing: Fisy::Observers::DriveObserver is for all drives, Fisy::Observers::DriveObserver::DriveInstance is for one root (e.g. `C:/` ), `Fisy::Observers::DriveObserver::DriveInstance::DirectoryObserver` is for single directory.