## Usage

1. Configure app in `config.json`
2. Start app with `config.json` file in the same directory

### `config.json`

```javascript
{
    // (optional) paths to other configs like this
    "configLocations": ["C:\\Users\\Me\\Documents", "C:\\Users\\You\\Documents"],

    // list of files or directories to sync
    "inSync": [
        {
            // (optional) If true,sync only from first location to others
            "oneDirection": false, //<- default

            // (optional) delays syncing
            "delayMs": 5000, //<- default

            // list of locations to synchronize
            "locations": [
                {
                    // drive is only option
                    "mediaType": "drive",

                    //(optional) could be found in logs...
                    "mediaId": "1a232b123b",

                    // path, could be without root if mediaId is present, directory paths should end with \ or / .directories are synced with all subdirectories.
                    "path": "/test/file.txt"
                },
                {
                    "mediaType": "drive",
                    "path": "C:\\test2\\file.txt"
                }
            ]
        }
    ]
}
```

## Build

Requirements:

* msvc++ 14.2 (VS 2019 version 16) or newer
* boost 1.73 installed at `$(SolutionDir)\boost\boost_1_73_0`. Precompiled binaries could be downloaded [here](https://sourceforge.net/projects/boost/files/boost-binaries/1.73.0/boost_1_73_0-msvc-14.2-64.exe) (If you choose to compile it yourself note that this project expects precompile libs to be in boost dir at `.\lib64-msvc-14.2`)

Then compile for 64 bit platform.

## Documentation

Documantation is generated with doxygen. This command will generate html pages in `docs/html` directory.

```
doxygen doxyfile
```