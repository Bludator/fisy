#pragma once
#include <iostream>

#include <boost\filesystem.hpp>
#include "../Settings.hpp"

namespace Fisy::Transfers
{
	/// <summary>
	/// Transfer function
	/// </summary>
	using Function = std::function<bool(const Location*, const Location*, bool)>;

	/// <summary>
	/// Metadata of file
	/// </summary>
	struct Metadata
	{
		time_t lastWrite;
		/// <summary>
		/// Indicate if is on connected media
		/// </summary>
		bool reachable;
	};

	/// <summary>
	/// Media type that provide methods to transfered file
	/// </summary>
	struct ITransferable
	{
		virtual ~ITransferable() = 0 {};

		/// <summary>
		/// Gets metadata
		/// </summary>
		/// <param name="location"></param>
		/// <returns></returns>
		virtual Metadata GetMetadata(const Location* location) = 0;
		/// <summary>
		/// Copy or move file from location to location, locations have same media type
		/// </summary>
		/// <param name="fromLocation"></param>
		/// <param name="toLocation"></param>
		/// <param name="move"></param>
		/// <returns></returns>
		virtual bool Transfer(const Location* fromLocation, const Location* toLocation, bool move) = 0;
		/// <summary>
		/// Remove file at location
		/// </summary>
		/// <param name="location"></param>
		virtual void Remove(const Location* location) = 0;
		/// <summary>
		/// Returns type of media
		/// </summary>
		/// <returns></returns>
		virtual MediaType GetType() = 0;

		/// <summary>
		///  Stream file at `location`
		/// </summary>
		/// <param name="location"></param>
		/// <returns></returns>
		virtual std::istream&& GetStream(const Location* location) = 0;

		/// <summary>
		/// Write data from provided input stream
		/// </summary>
		/// <param name="location"></param>
		/// <param name="input"></param>
		virtual void SetStream(const Location* location, std::istream&& input) = 0;
	};
}