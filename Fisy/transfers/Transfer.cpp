#include "Transfer.hpp"

namespace Fisy::Transfers
{
	std::unordered_map<std::pair<MediaType, MediaType>, Function, boost::hash<std::pair<MediaType, MediaType>>> Transfer::map;

	Transfer::Transfer(std::vector<std::unique_ptr<Fisy::Transfers::ITransferable>> transverers)
	{
		for (auto& transferer : transverers)
		{
			this->transverers.emplace(transferer->GetType(), std::move(transferer));
		}
	}

	Metadata Transfer::GetMetadata(const Location* location)
	{
		if (location == nullptr)
		{
			return Metadata{ .lastWrite = time_t(), .reachable = true };
		}
		return transverers[location->mediaType]->GetMetadata(location);
	}

	void Transfer::Remove(const Location* location)
	{
		return transverers[location->mediaType]->Remove(location);
	}
	void Transfer::Proceed(Pipeline::Data data)
	{
		if (!data.to)
		{
			Remove(&*data.from);
		}
		else
		{
			auto fx = map.find({ data.from->mediaType, data.to->mediaType });
			if (fx == map.end())
			{
				transverers[data.from->mediaType]->SetStream(&*data.to, transverers[data.from->mediaType]->GetStream(&*data.from));
			}
			else
			{
				if (!fx->second(&*data.from, &*data.to, data.move))
				{
					return;
				}
			}
		}
		nextStage->Proceed(std::move(data));
	}
}