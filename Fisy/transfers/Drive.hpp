#pragma once
#include <boost/filesystem.hpp>

#include "DriveBase.hpp"
#include "ITransferable.hpp"
namespace Fisy::Transfers
{
	namespace Drive
	{
		boost::filesystem::path GetPath(const Location* location);
		std::unique_ptr<Location> GetLocation(boost::filesystem::path pathStr);

		/// <summary>
		/// Gets id of drive. Id is "volume serial number that the operating system assigns when a hard disk is formatted."
		/// </summary>
		/// <seealso cref="https://docs.microsoft.com/en-us/windows/win32/api/fileapi/nf-fileapi-getvolumeinformationw"/>
		/// <param name="root">letter of drive e.g. `C:\`</param>
		/// <returns>hex number</returns>
		std::string RootToId(std::string root);

		/// <summary>
		///
		/// </summary>
		/// <param name="id">hex number</param>
		/// <returns>letter of drive e.g. `C:\`</returns>
		std::string IdToRoot(std::string id);

		inline bool isSubPath(std::wstring prefix, std::wstring path)
		{
			if (prefix.empty()) return true;
			return prefix.length() <= path.length() &&
				std::equal(prefix.begin(), prefix.end(), path.begin()) &&
				*prefix.rbegin() == boost::filesystem::path::preferred_separator;
		}

		inline bool isSubPath(boost::filesystem::path prefix, boost::filesystem::path path)
		{
			if (prefix.empty()) return true;
			/*if (prefix.filename_is_dot())
			{
				prefix.remove_filename();
			}*/
			return  std::distance(prefix.begin(), prefix.end()) <= std::distance(path.begin(), path.end()) &&
				std::equal(prefix.begin(), prefix.end(), path.begin());
		}
	}
	struct DriveTransfer : public DriveBase
	{
		~DriveTransfer() override {};
		virtual boost::filesystem::path GetPath(const Location* location) override
		{
			return Drive::GetPath(location);
		};
	};
}