#pragma once
#include "ITransferable.hpp"
namespace Fisy::Transfers
{
	/// <summary>
	/// Base class for implementations of  `ITransferable` for drives
	/// </summary>
	struct DriveBase : public virtual ITransferable
	{
	private:
		using Path = boost::filesystem::path;
	public:

		DriveBase();
		~DriveBase() = 0 {};

		bool Transfer(const Location* from, const Location* to, bool move) override;
		Metadata GetMetadata(const Location* location) override;
		void Remove(const Location* location) override;

		std::istream&& GetStream(const Location* location) override;
		void SetStream(const Location* location, std::istream&& input) override;

		virtual Path GetPath(const Location* location) = 0;
		virtual MediaType GetType() override { return MediaType::Drive; };
	};
}