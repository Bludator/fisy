#include <Windows.h>
#include <sstream>
#include <boost/nowide/convert.hpp>

#include "Drive.hpp"

namespace fs = boost::filesystem;
namespace nowide = boost::nowide;

namespace Fisy::Transfers::Drive
{
	fs::path fixPath(fs::path path)
	{
		path = path.lexically_normal();
		path.make_preferred();
		if (path.filename_is_dot()) { path.remove_filename(); }
		return path;
	}

	fs::path GetPath(const Location* location)
	{
		//?todo: check long paths
		return fixPath(fs::path(IdToRoot(location->mediaId) + location->path));
	}

	std::unique_ptr<Location> GetLocation(fs::path  path)
	{
		path = fixPath(path);
		return std::make_unique<Location>(
			Location{
				.mediaType = MediaType::Drive,
				.mediaId = RootToId(path.root_path().string()),
				.path = path.relative_path().string(),
				.settings = nullptr
			});
	}
	std::unordered_map<std::string, std::string> MapIdToRoot{};
	std::string RootToId(std::string root)
	{
		DWORD SerialNumber = 0;
		if (GetVolumeInformation(nowide::widen(root).c_str(), NULL, NULL, &SerialNumber, NULL, NULL, NULL, NULL))
		{
			std::stringstream s;
			s << std::hex << SerialNumber;
			MapIdToRoot.insert({ s.str(),root });
			return s.str();
		}
		else return "";
	}
	std::string IdToRoot(std::string id)
	{
		auto it = MapIdToRoot.find(id);
		return it != MapIdToRoot.end() ? it->second : std::string();
	}
}