#pragma once
#include <unordered_map>
#include <memory>
#include <boost/container_hash/hash.hpp>

#include "ITransferable.hpp"
#include "../pipeline/IStage.hpp"
#include "../Settings.hpp"

namespace Fisy::Transfers
{
	/// <summary>
	/// Wraps all `ITransferable` delegate all work to them
	/// </summary>
	class Transfer :public Fisy::Pipeline::SimpleStage
	{
	public:
		/// <summary>
		/// lists Transfer functions
		/// </summary>
		static std::unordered_map<std::pair<MediaType, MediaType>, Function, boost::hash<std::pair<MediaType, MediaType>>> map;

		/// <summary>
		/// Create `Transfer` with all known `ITransferable`
		/// </summary>
		/// <param name="transverers"></param>
		Transfer(std::vector<std::unique_ptr<Fisy::Transfers::ITransferable>> transverers);

		/// <summary>
		/// Calls `ITransferable::GetMetadata` on appropriate `ITransferable`
		/// </summary>
		/// <param name="location">location of file which metadata will be returned</param>
		/// <returns></returns>
		Metadata GetMetadata(const Location* location);

		/// <summary>
		/// Calls `Remove` on appropriate `ITransferable`
		/// </summary>
		/// <param name="location">location to file to remove</param>
		void Remove(const Location* location);

		/// <summary>
		/// Finds best way to transfer files. Checks `map`, if no function found transfer via streams.
		/// </summary>
		/// <param name="data"></param>
		virtual void Proceed(Pipeline::Data data) override;

	private:
		/// <summary>
		/// Lists `ITransferable` by `MediaType`
		/// </summary>
		std::unordered_map<MediaType, std::unique_ptr<ITransferable>> transverers;
	};
}