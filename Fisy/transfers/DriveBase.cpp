#include <chrono>
#include <iomanip>
#include <boost/filesystem.hpp>
#include <boost/nowide/fstream.hpp>

#include "Transfer.hpp"
#include "DriveBase.hpp"

namespace fs = boost::filesystem;
namespace nowide = boost::nowide;

namespace Fisy::Transfers
{
	DriveBase::DriveBase()
	{
		Transfer::map.insert({
			{ MediaType::Drive, MediaType::Drive },
			[this](const Location* fromLocation, const Location* toLocation, bool move) -> bool
				{return Transfer(fromLocation,toLocation, move); }
			});//?todo: make transfer static?
	}
	std::istream&& DriveBase::GetStream(const Location* location)
	{
		Path path = GetPath(location);
		return std::move(nowide::ifstream(path));
	}

	void DriveBase::SetStream(const Location* location, std::istream&& input)
	{
		Path path = GetPath(location);
		nowide::ofstream os(path);
		os << input.rdbuf();
	}

	Metadata DriveBase::GetMetadata(const Location* location)
	{
		assert(location->mediaType == MediaType::Drive);
		auto path = GetPath(location);
		if (fs::exists(path))
		{
			return Metadata{ .lastWrite = fs::last_write_time(path), .reachable = true };
		}
		else
		{
			//?todo: reachable, or maybe not if we don't update location recursively
			return Metadata{ .lastWrite = time_t(), .reachable = fs::exists(path.parent_path()) };
		}
	}

	void DriveBase::Remove(const Location* location)
	{
		Path path = GetPath(location);
		fs::remove(path);
	}

	bool DriveBase::Transfer(const Location* fromLocation, const Location* toLocation, bool move)
	{
		assert(fromLocation != nullptr);
		if (toLocation == nullptr)
		{
			assert(!move);
			Remove(fromLocation);
			return true;
		}
		Path from = GetPath(fromLocation);
		Path to = GetPath(toLocation);
		if (!move)
		{
			if (fs::is_directory(from))
			{
				if (fs::exists(to))
				{
					return false; //?todo:
				}
				fs::copy(from, to);
			}
			else
			{
				fs::copy_file(from, to, fs::copy_option::overwrite_if_exists);
			}
		}
		else
		{
			fs::rename(from, to);
		}
		fs::last_write_time(to, fs::last_write_time(from));
		// todo: error handling
		return  true;
	}
}