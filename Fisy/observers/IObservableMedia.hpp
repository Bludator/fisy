#pragma once
#include <memory>

#include "../Settings.hpp"
#include "../pipeline/IStage.hpp"
namespace Fisy::Observers
{
	/// <summary>
	/// Pure abstract class representing media (e.g. drives, mtp) that could be observed for changes of locations/files
	/// </summary>
	struct IObservableMedia
	{
		using IStage = Pipeline::IStage;
		virtual ~IObservableMedia() = 0 {};

		/// <summary>
		/// Remove all observing locations
		/// </summary>
		virtual void ResetLocations() = 0;

		/// <summary>
		/// Start observing location ~ starts `proceed`ing to stages set by `NextOnChange` and `NextOnConnect`
		/// </summary>
		/// <param name="location">location to observe for changes</param>
		virtual void AddLocation(const Location* location) = 0;

		/// <summary>
		/// Remove location from observing
		/// </summary>
		/// <param name="location">location to stop observing for changes</param>
		virtual void RemoveLocation(const Location* location) = 0;

		/// <summary>
		/// Sets next stage for changes that happened in real time. If already set, overrides previously saved `IStage`.
		/// </summary>
		/// <param name="nextStage">next `IStage` for online changes</param>
		/// <returns>pointer to nextStage</returns>
		virtual Pipeline::IStage* NextOnChange(std::shared_ptr<Pipeline::IStage> nextStage) = 0;

		/// <summary>
		/// Sets next stage for changes that may happened on newly connected media (in past). If already set, overrides previously saved `IStage`.
		/// </summary>
		/// <param name="nextStage">next `IStage` for offline changes</param>
		/// <returns>pointer to nextStage</returns>
		virtual Pipeline::IStage* NextOnConnect(std::shared_ptr<Pipeline::IStage> nextStage) = 0;

		/// <summary>
		/// Gets media type
		/// </summary>
		/// <returns>media type that this instance represents</returns>
		virtual MediaType GetType() = 0;
	};
}