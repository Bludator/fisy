#pragma once
#include <atomic>
#include <thread>
#include <string>
#include <vector>
#include <Windows.h>
#include <boost/filesystem.hpp>

#include "MediaObserverBase.hpp"
namespace Fisy::Observers
{
	/// <summary>
	/// Extends/implements <see cref="MediaObserverBase"/> with drive support.
	/// Observe all drives, for connect, disconnect and files changes
	/// </summary>
	class DriveObserver final :public MediaObserverBase
	{
	protected:

		/// <summary>
		/// Extends <see cref="MediaObserverBase::Instance"/> with drive support. Observe one specific drive for files changes
		/// </summary>
		class DriveInstance final :public Instance
		{
		private:
			using Path = boost::filesystem::path;

			/// <summary>
			/// Observe one specific folder for files changes
			/// </summary>
			class DirectoryObserver
			{
			public:
				DirectoryObserver(DriveInstance& instance, Path directory);
				~DirectoryObserver();
				Path GetPath() { return directoryPath; }
				const std::vector<std::pair<std::wstring, const Location*>>& GetLocations()
				{
					return pathToLocation;
				}
				void AddLocation(const Location* location);
				void RemoveLocation(const Location* location);

			private:
				DriveInstance& driveInstance;
				/// <summary>
				/// paths are relative to the path of the directory, every path should be valid if concatenated with `path`
				/// </summary>
				std::vector<std::pair<std::wstring, const Location*>> pathToLocation;
				Path directoryPath;
				HANDLE directoryHandle = nullptr;
				OVERLAPPED	overlappedStruct = {};//init to zero
				std::array<FILE_NOTIFY_INFORMATION[1024], 2> buffer; //max 64KB for network locations
				char waitingBuffer = 0;

				/// <summary>
				/// Gets handle to directory
				/// </summary>
				void InitDirectory();
				/// <summary>
				/// Register notifications
				/// </summary>
				/// <param name="recursive"></param>
				void Register(bool recursive = true);
				static void CALLBACK OnNotification(DWORD dwErrorCode, DWORD dwNumberOfBytesTransfered, LPOVERLAPPED lpOverlapped);
				void ProcessNotification();
				void FilterNotification(std::wstring from, std::wstring to, bool move = false);

				/// <summary>
				/// Create location based on relative path and triggering location
				/// </summary>
				std::unique_ptr<Location> CreateLocation(std::wstring path, const Location& trigger);
				std::vector<const Location*> FindPrefixes(std::wstring path);
			};
		public:
			DriveInstance(std::string id, MediaObserverBase& mediaType);
			~DriveInstance()override;

			virtual void AddLocation(const Location* locations) override;
			virtual void ResetLocations() override;
			virtual void RemoveLocation(const Location* location) override;
			virtual std::vector<const Location*> GetLocations() override;
		private:
			std::map<Path, std::unique_ptr<DirectoryObserver>, std::greater<Path>> directoryObservers;

			DirectoryObserver* ObserveDirectory(Path directoryPath);
			void StopObserving(DirectoryObserver* directoryHandle);
		};
	public:
		/// <summary>
		/// Starts observing drive connection and disconnection in new thread
		/// </summary>
		DriveObserver();
		~DriveObserver() override;

		/// <summary>
		/// Check if drive is available/active
		/// </summary>
		/// <param name="letter">letter of drive</param>
		/// <returns></returns>
		bool IsDriveLetterAvailable(char letter);

		/// <summary>
		/// Recursively walk subtree.
		/// </summary>
		/// <param name="locToIterate"></param>
		/// <param name="action"></param>
		virtual void ForEachFileRecursively(const Location* locToIterate, std::function<void(Location)> action) override;

		virtual MediaType GetType() override
		{
			return MediaType::Drive;
		};
	private:
		/// <summary>
		/// Observe drive connection and disconnection
		/// </summary>
		void Observe();
		DWORD lastMask = 0;
		std::thread wt;
		std::atomic<bool> terminate = false;
	};
}