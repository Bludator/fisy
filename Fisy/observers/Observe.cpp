#include "Observe.hpp"

namespace Fisy::Observers {
	Observe::Observe(std::vector<std::unique_ptr<Fisy::Observers::IObservableMedia>> mediaObservers) :mediaObservers()
	{
		for (auto& mediaObserver : mediaObservers)
		{
			this->mediaObservers.emplace(mediaObserver->GetType(), std::move(mediaObserver));
		}
	}

	void Observe::Add(const Settings* settings)
	{
		for (auto&& location : settings->locations)
		{
			mediaObservers[location->mediaType]->AddLocation(&*location);
		}
	}

	void Observe::Remove(const Settings* settings)
	{
		for (auto&& location : settings->locations)
		{
			mediaObservers[location->mediaType]->RemoveLocation(&*location);
		}
	}

	void Observe::Reset()
	{
		for (auto&& observer : mediaObservers)
		{
			observer.second->ResetLocations();
		}
	}

	Pipeline::IStage* Observe::NextOnChange(std::shared_ptr<Pipeline::IStage> nextStage)
	{
		for (auto&& observer : mediaObservers)
		{
			observer.second->NextOnChange(nextStage);
		}
		return &*nextStage;
	};

	Pipeline::IStage* Observe::NextOnConnect(std::shared_ptr<Pipeline::IStage> nextStage)
	{
		for (auto&& observer : mediaObservers)
		{
			observer.second->NextOnConnect(nextStage);
		}
		return &*nextStage;
	};
}