#include <Windows.h>
#include <iomanip>
#include <sstream>
#include <string>
#include <thread>
#include <algorithm>
#include <boost/filesystem.hpp>
#include <boost/nowide/convert.hpp>

#include "../UserException.hpp"
#include "DriveObserver.hpp"
#include "../transfers/Drive.hpp"

#define COMMANDLINE 1

using namespace Fisy::Transfers::Drive;
namespace fs = boost::filesystem;
namespace nowide = boost::nowide;

namespace Fisy::Observers
{
	DriveObserver::DriveObserver()
	{
		//todo: skips controlling changes, if the changes are added before the new thread is created
		//next iteration adds all online disks
		lastMask = 0;
		wt = std::move(std::thread(&DriveObserver::Observe, this));
	}

	DriveObserver::~DriveObserver()
	{
		//inelegant?
		terminate = true;
		wt.join();
	}
	void DriveObserver::Observe()
	{
#ifdef COMMANDLINE
		while (!terminate)
		{
			DWORD currentMask = GetLogicalDrives();
			DWORD changes = currentMask ^ lastMask;
			if (changes)
			{
				for (char i = 0; i < sizeof(changes) * 8; i++)
				{
					if ((1 << i) & changes)
					{
						if ((1 << i) & currentMask)
						{
							OnMediaAddedHandler(std::make_unique<DriveInstance>(RootToId(std::string(1, i + 'A') + ":\\"), *this));
						}
						else
						{
							OnMediaRemovedHandler(RootToId(std::string(1, i + 'A') + ":\\"));
						}
					}
				}
				lastMask = currentMask;
			}
			Synchronize();
			SleepEx(1000, TRUE);
			//std::this_thread::sleep_for(std::chrono::seconds(1000));
		}
#endif // COMMANDLINE

		//?todo: in service
		// it require handle to service/window
		// RegisterDeviceNotification
	}

	bool DriveObserver::IsDriveLetterAvailable(char letter)
	{
		DWORD mask = GetLogicalDrives();
		return 1 & (mask >> (letter - 'A'));
	}

	void DriveObserver::ForEachFileRecursively(const Location* locToIterate, std::function<void(Location)> action)
	{
		auto path = GetPath(locToIterate);
		if (fs::is_directory(path))
		{
			for (auto& i : fs::recursive_directory_iterator(path))
			{
				Location newLocation = *locToIterate;
				newLocation.path = i.path().relative_path().string();
				action(newLocation);
			}
		}
		else
		{
			action(*locToIterate);
		}
	}

	DriveObserver::DriveInstance::DriveInstance(std::string id, MediaObserverBase& mediaType) :
		MediaObserverBase::Instance::Instance(id, mediaType)
	{
	}

	DriveObserver::DriveInstance::~DriveInstance()
	{
	}

	std::vector<const Location*> DriveObserver::DriveInstance::GetLocations()
	{
		std::vector<const Location*>v;
		for (auto& [path, directoryObserver] : directoryObservers)
		{
			auto dir = directoryObserver->GetLocations();
			for (auto it = dir.begin(); it != dir.end(); ++it) {
				v.push_back(it->second);
			}
		}
		return v;
	}

	void DriveObserver::DriveInstance::AddLocation(const Location* location)
	{
		auto path = GetPath(location);
		auto it = directoryObservers.lower_bound(path);

		if (it != directoryObservers.end())
		{
			if (isSubPath(it->first, path))
			{
				it->second->AddLocation(location);
				return;
			}
		}

		if (!fs::is_directory(path))
		{
			path = path.remove_filename();
		}
		ObserveDirectory(path)->AddLocation(location);
	}
	void DriveObserver::DriveInstance::ResetLocations()
	{
		for (auto& [path, directoryObserver] : directoryObservers)
		{
			StopObserving(&*directoryObserver);
		}
	}
	void DriveObserver::DriveInstance::RemoveLocation(const Location* location)
	{
		auto path = GetPath(location);
		auto it = directoryObservers.lower_bound(path.string());
		if (it == directoryObservers.end())return;

		it->second->RemoveLocation(location);

		if (it->second->GetLocations().size() == 0)
		{
			StopObserving(&*it->second);
		}
	}
	void DriveObserver::DriveInstance::StopObserving(DirectoryObserver* directoryObserver)
	{
		auto it = std::find_if(directoryObservers.begin(), directoryObservers.end(), [&directoryObserver](auto& pair) {return directoryObserver == &*pair.second; });
		if (it != directoryObservers.end())
		{
			directoryObservers.erase(it);
		}
	}

	DriveObserver::DriveInstance::DirectoryObserver* DriveObserver::DriveInstance::ObserveDirectory(Path directoryPath)
	{
		//remove observed subdirectories
		auto observer = std::make_unique<DirectoryObserver>(*this, directoryPath);

		auto it = directoryObservers.lower_bound(directoryPath);
		if (it != directoryObservers.begin())
		{
			auto reverseIt = std::make_reverse_iterator(it);
			while (reverseIt != directoryObservers.rend() && isSubPath(directoryPath, reverseIt->first))
			{
				for (auto& [path, location] : reverseIt->second->GetLocations())
				{
					observer->AddLocation(location);
				}
				reverseIt++;
				// Reverse iterator is kind of broken: here points to the element we would like to erase..... :-(
			}
			auto reassigned = reverseIt.base();
			while (reassigned != it)
			{
				auto toErase = reassigned;
				reassigned++;
				directoryObservers.erase(toErase);
			}
		}
		return &*directoryObservers.emplace(directoryPath, std::move(observer)).first->second;
	}

	DriveObserver::DriveInstance::DirectoryObserver::DirectoryObserver(DriveInstance& driveInstance, Path directory) :
		directoryPath(directory),
		driveInstance(driveInstance)
	{
		// The hEvent member of the OVERLAPPED structure is not used by the system, so you can use it yourself.
		overlappedStruct.hEvent = this;
	}

	DriveObserver::DriveInstance::DirectoryObserver::~DirectoryObserver()
	{
		CancelIoEx(directoryHandle, &overlappedStruct);
		//?todo: wait to invoke OnNotification
	}

	void DriveObserver::DriveInstance::DirectoryObserver::AddLocation(const Location* location)
	{
		if (directoryHandle == nullptr)
		{
			InitDirectory();
			Register();
		}
		auto path = Transfers::Drive::GetPath(location);
		pathToLocation.push_back({ path.wstring().substr(directoryPath.wstring().length()), location });
	}

	void DriveObserver::DriveInstance::DirectoryObserver::RemoveLocation(const Location* location)
	{
		auto iterator = std::find_if(pathToLocation.begin(), pathToLocation.end(), [&location](auto& pair) {return location == &*pair.second; });
		if (iterator != pathToLocation.end())
		{
			pathToLocation.erase(iterator);
		}
	}

	void DriveObserver::DriveInstance::DirectoryObserver::InitDirectory()
	{
		directoryHandle = CreateFileW(
			directoryPath.c_str(),
			FILE_LIST_DIRECTORY,
			FILE_SHARE_WRITE | FILE_SHARE_READ | FILE_SHARE_DELETE,
			NULL,
			OPEN_EXISTING,
			FILE_FLAG_BACKUP_SEMANTICS | FILE_FLAG_OVERLAPPED, //must use backup semantic with directories
			NULL);

		if (directoryHandle == INVALID_HANDLE_VALUE)
		{
			DWORD errorCode = GetLastError();
			if (errorCode == ERROR_FILE_NOT_FOUND)
			{
				throw UserException("directory to observe not found");
			}
			if (errorCode == ERROR_PATH_NOT_FOUND)
			{
				throw UserException("invalid path");
			}
			throw std::exception("cannot register directory to observe, failed with error:" + errorCode);
		}
	}

	void DriveObserver::DriveInstance::DirectoryObserver::Register(bool recursive)
	{
		DWORD dwBytesReturned = 0;
		BOOL succes = ReadDirectoryChangesW(
			directoryHandle,
			(LPVOID)&buffer[waitingBuffer],
			sizeof(buffer[waitingBuffer]),
			recursive,
			FILE_NOTIFY_CHANGE_LAST_WRITE | FILE_NOTIFY_CHANGE_FILE_NAME | FILE_NOTIFY_CHANGE_DIR_NAME,
			&dwBytesReturned, // for async calls irrelevant...
			&overlappedStruct,
			&OnNotification);

		if (!succes)
		{
			DWORD errorCode = GetLastError();
			if (ERROR_INVALID_FUNCTION)//not supported in this location
			{
			}
			throw std::exception("cannot register observing, failed with error:" + errorCode);
		}
	}

	void CALLBACK DriveObserver::DriveInstance::DirectoryObserver::OnNotification(DWORD ErrorCode, DWORD BytesTransfered, LPOVERLAPPED Overlapped)
	{
		DirectoryObserver* instance = (DirectoryObserver*)Overlapped->hEvent;

		if (ErrorCode == ERROR_OPERATION_ABORTED)
		{
			//?todo: aborted
			return;
		}

		if (!BytesTransfered)
		{
			//possible overflow, or big problem, docs:
			//If the number of bytes transferred is zero, the buffer was either too large for the system to allocate or too small to provide detailed information on all the changes that occurred in the directory or subtree.*/
			return;
		}
		//switch buffers
		instance->waitingBuffer = !instance->waitingBuffer;

		instance->Register();
		instance->ProcessNotification();
	}

	enum class Action :DWORD
	{
		Add = FILE_ACTION_ADDED,
		Remove = FILE_ACTION_REMOVED,
		Write = FILE_ACTION_MODIFIED,
		OldName = FILE_ACTION_RENAMED_OLD_NAME,
		NewName = FILE_ACTION_RENAMED_NEW_NAME
	};

	void DriveObserver::DriveInstance::DirectoryObserver::ProcessNotification()
	{
		size_t offset = 0;
		FILE_NOTIFY_INFORMATION* record;
		std::wstring oldName{};
		do
		{
			record = (FILE_NOTIFY_INFORMATION*)((char*)buffer[!waitingBuffer] + offset);
			//beware: path could be in 8.3 (short name) form, but we ignore it
			std::wstring path(record->FileName, record->FileNameLength / 2);
			Action action = static_cast<Action>(record->Action);
			offset += record->NextEntryOffset;

			switch (action)
			{
			case Action::Add:
				FilterNotification(L"", path);
				break;
			case Action::Remove:
				FilterNotification(path, L"");//if next notification is add -> move
				break;
			case Action::Write:
				//todo: filter out directory write as they are better covered by others notifications
				FilterNotification(L"", path);
				break;
			case Action::OldName:
				oldName = path;
				continue;
				break;
			case Action::NewName:
				FilterNotification(oldName, path, true);
				break;
			default:
				break;
			}
			oldName.clear();
		} while (record->NextEntryOffset != 0);
	}

	std::vector<const Location*> DriveObserver::DriveInstance::DirectoryObserver::FindPrefixes(std::wstring path)
	{
		std::vector<const Location*> prefixes;

		for (auto&& [prefix, location] : pathToLocation)
		{
			if (isSubPath(prefix, path) || prefix == path)
			{
				prefixes.push_back(location);
			}
		}
		return prefixes;
	}

	std::unique_ptr<Location> DriveObserver::DriveInstance::DirectoryObserver::CreateLocation(std::wstring path, const Location& trigger)
	{
		if (path.size() == 1)return nullptr;
		auto newLocation = std::make_unique<Location>(trigger);
		newLocation->path = directoryPath.relative_path().string() + nowide::narrow(path);
		return std::move(newLocation);
	}

	void DriveObserver::DriveInstance::DirectoryObserver::FilterNotification(std::wstring from, std::wstring to, bool move)
	{
		//?todo: once the move is supported, this will not work
		std::wstring path = to.empty() ? from : to;
		path = fs::path::preferred_separator + path;
		for (auto& location : FindPrefixes(path))
		{
			driveInstance.GetnextStage()->Proceed(
				Pipeline::Data{
					.from = CreateLocation(fs::path::preferred_separator + from,*location),
					.to = CreateLocation(fs::path::preferred_separator + to,*location),
					.trigger = location,
					.move = false
				});
		}
	}
}