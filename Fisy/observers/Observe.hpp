#pragma once
#include <unordered_map>
#include <memory>

#include "../pipeline/IStage.hpp"
#include "IObservableMedia.hpp"

namespace Fisy::Observers
{
	/// <summary>
	/// Wraps all `IObservableMedia`, delegate all work to them, partially imitate `IObservableMedia`
	/// </summary>
	class Observe
	{
	public:
		/// <summary>
		/// Construct `Observe` with all known `IObservableMedia` i.e. for every `MediaType`
		/// </summary>
		/// <param name="mediaObservers"> all known `IObservableMedia`</param>
		Observe(std::vector<std::unique_ptr<Fisy::Observers::IObservableMedia>> mediaObservers);

		/// <summary>
		/// Start observing all locations specified in `settings` object, call `AddLocation` on appropriate `IObservableMedia`
		/// </summary>
		/// <param name="settings"></param>
		void Add(const Settings* settings);

		/// <summary>
		/// Remove all locations specified in `settings` object from observing, call `RemoveLocation` on appropriate `IObservableMedia`
		/// </summary>
		/// <param name="settings"></param>
		void Remove(const Settings* settings);

		/// <summary>
		/// Calls `Reset` on all provided `IObservableMedia`
		/// </summary>
		void Reset();

		/// <summary>
		/// Calls `NextOnChange` on all provided `IObservableMedia`
		/// </summary>
		/// <param name="nextStage">next `IStage` for online changes</param>
		/// <returns>pointer to nextStage</returns>
		Pipeline::IStage* NextOnChange(std::shared_ptr<Pipeline::IStage> nextStage);

		/// <summary>
		/// Calls `NextOnConnect` on all provided `IObservableMedia`
		/// </summary>
		/// <param name="nextStage">next `IStage` for offline changes</param>
		/// <returns>pointer to nextStage</returns>
		Pipeline::IStage* NextOnConnect(std::shared_ptr<Pipeline::IStage> nextStage);

	private:
		std::unordered_map<MediaType, std::unique_ptr<Fisy::Observers::IObservableMedia>> mediaObservers;
	};
}