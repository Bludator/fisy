#pragma once
#include <vector>
#include <string>
#include <memory>
#include <unordered_map>
#include <mutex>

#include "../Settings.hpp"
#include "../pipeline/IStage.hpp"
#include "IObservableMedia.hpp"

namespace Fisy::Observers
{
	/// <summary>
	/// Base class for observers. Manages instances/devices of one `MediaType` and their connection and disconnection
	/// </summary>
	class  MediaObserverBase : public virtual IObservableMedia
	{
	protected:
		/// <summary>
		/// Represents one instance/device in `MediaObserverBase`
		/// </summary>
		class  Instance
		{
		public:
			/// <summary>
			/// Initializes a new instance of the <see cref="MediaInstance"/> class.
			/// </summary>
			/// <param name="Id">The identifier.</param>
			Instance(std::string id, MediaObserverBase& media) :id(id), media(media) {};
			virtual ~Instance() = 0 {};

			/// <summary>
			/// Starts observing location for changes
			/// </summary>
			/// <param name="locations">location to observe</param>
			virtual void AddLocation(const Location* locations) = 0;

			/// <summary>
			/// Remove all locations
			/// </summary>
			virtual void ResetLocations() = 0;

			/// <summary>
			/// Stops observing location
			/// </summary>
			/// <param name="location"></param>
			virtual void RemoveLocation(const Location* location) = 0;

			/// <summary>
			/// Get observed locations
			/// </summary>
			/// <returns>observed locations</returns>
			virtual std::vector<const Location*> GetLocations() = 0;

			/// <summary>
			/// Id of this media instance
			/// </summary>
			const std::string id;

		protected:
			/// <summary>
			/// `IStage` to `proceed` on detected change
			/// </summary>
			/// <returns>next (on-line) stage /returns>
			std::shared_ptr<IStage>& GetnextStage()
			{
				return media.nextOnlineStage;
			};
			/// <summary>
			/// Reference to `MediaObserverBase` class which this class is `::Instance` of
			/// </summary>
			MediaObserverBase& media;
		};
	public:
		/// <summary>
		/// Sets next stage for changes that happened in real time. If already set, overrides previously saved `IStage`.
		/// </summary>
		/// <param name="nextStage">next `IStage` for online changes</param>
		/// <returns>pointer to nextStage</returns>
		virtual IStage* NextOnChange(std::shared_ptr<IStage> nextStage) override
		{
			this->nextOnlineStage = std::move(nextStage);
			return &*this->nextOnlineStage;
		};

		/// <summary>
		/// Sets next stage for changes that may happened on disconnected media (in past). If already set, overrides previously saved `IStage`.
		/// </summary>
		/// <param name="nextStage">next `IStage` for offline changes</param>
		/// <returns>pointer to nextStage</returns>
		virtual IStage* NextOnConnect(std::shared_ptr<IStage> nextStage) override
		{
			this->nextOfflineStage = std::move(nextStage);
			return &*this->nextOfflineStage;
		};

		virtual ~MediaObserverBase() = 0 {};

		/// <summary>
		/// Starts observing location in corresponding `Instance` if the `Instance` is connected or store it to later on when the `Instance` connects
		/// </summary>
		/// <param name="locations"></param>
		virtual void AddLocation(const Location* location) override
		{
			std::lock_guard lock(mtx);
			addQueue.push_back(location);
		}

		/// <summary>
		/// Stops observing location
		/// </summary>
		/// <param name="location"></param>
		virtual void RemoveLocation(const Location* location) override
		{
			std::lock_guard lock(mtx);
			removeQueue.push_back(location);
		}

		/// <summary>
		/// Remove all observing locations. Propagate this call to all `Instance`s this `MediaObserverBase` manages
		/// </summary>
		virtual void ResetLocations() override
		{
			std::lock_guard lock(mtx);
			isReset = true;
		}

	protected:

		/// <summary>
		/// Function called when new `Instance` is added.
		/// </summary>
		void OnMediaAddedHandler(std::unique_ptr <Instance> media);

		/// <summary>
		/// Function called when `Instance` is removed.
		/// </summary>
		void OnMediaRemovedHandler(std::string id);

		/// <summary>
		/// Recursively walks `locToIterate` and calls `action` on every found file
		/// </summary>
		/// <param name="locToIterate"></param>
		/// <param name="action"></param>
		virtual void ForEachFileRecursively(const Location* locToIterate, std::function<void(Location)>action) = 0;

		/// <summary>
		/// Process calls to `AddLocation`, `RemoveLocation` and `ResetLocations`
		/// </summary>
		void Synchronize();

	private:
		//synchronization queues for `Synchronize`
		std::mutex mtx;
		std::vector<const Location*> addQueue;
		std::vector<const Location*> removeQueue;
		bool isReset = false;

		/// <summary>
		/// thread unsafe implementation of `AddLocation`
		/// </summary>
		/// <param name="location"></param>
		void StartObserving(const Location* location);

		/// <summary>
		/// thread unsafe implementation of `RemoveLocation`
		/// </summary>
		/// <param name="location"></param>
		void StopObserving(const Location* location);

		/// <summary>
		/// Set by `NextOnConnect`. For newly connected locations.
		/// </summary>
		std::shared_ptr<IStage> nextOfflineStage;

		/// <summary>
		/// Set by `NextOnChange`. For real time changes.
		/// </summary>
		std::shared_ptr<IStage> nextOnlineStage;

		/// <summary>
		///	Locations of disconnected `MediaObserverBase::Instances`
		/// </summary>
		std::unordered_map<std::string, std::vector<const Location*>> inactiveLocations;

		/// <summary>
		/// Maps id to active media `Instance`s
		/// </summary>
		std::unordered_map<std::string, std::unique_ptr<Instance>> instances;
	};
}