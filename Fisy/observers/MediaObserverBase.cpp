#include "MediaObserverBase.hpp"
#include <algorithm>

namespace  Fisy::Observers {
	void MediaObserverBase::StartObserving(const Location* location)
	{
		auto activeLocation = instances.find(location->mediaId);
		if (activeLocation == instances.end()) //inactive location?
		{
			auto inactive = inactiveLocations.find(location->mediaId);
			if (inactive == inactiveLocations.end()) // new inactive media?
			{
				inactiveLocations.emplace(location->mediaId, std::vector<const Location*>{ location });
			}
			else
			{
				inactive->second.push_back(location);
			}
		}
		else
		{
			instances[location->mediaId]->AddLocation(location);
			ForEachFileRecursively(location, [this, &location](Location loc) {
				nextOfflineStage->Proceed(Pipeline::Data
					{
						.from = nullptr,
						.to = std::make_unique<Location>(loc),
						.trigger = location,
						.move = false });
			});
		}
	}

	void MediaObserverBase::StopObserving(const Location* location)
	{
		auto activeLocation = instances.find(location->mediaId);
		if (activeLocation == instances.end())
		{
			auto inactive = inactiveLocations.find(location->mediaId);
			if (inactive == inactiveLocations.end()) return;

			auto foundLocation = std::find(inactive->second.begin(), inactive->second.end(), location);
			if (foundLocation == inactive->second.end()) return;

			inactive->second.erase(foundLocation);

			if (inactive->second.empty())
			{
				inactiveLocations.erase(inactive);
			}
		}
		else
		{
			instances[location->mediaId]->RemoveLocation(location);
		}
	}

	void MediaObserverBase::OnMediaAddedHandler(std::unique_ptr<Instance> media)
	{
		std::lock_guard lock(mtx);
		for (auto& location : inactiveLocations[media->id])
		{
			media->AddLocation(location);
			for (auto& l : location->settings->locations)
			{//!todo: 
				ForEachFileRecursively(&*l, [this, &l](Location loc) {
					nextOfflineStage->Proceed(Pipeline::Data
						{
							.from = nullptr,
							.to = std::make_unique<Location>(loc),
							.trigger = &*l,
							.move = false });
				});
			}
		}
		instances.emplace(media->id, std::move(media));
	}

	void MediaObserverBase::OnMediaRemovedHandler(std::string id)
	{
		std::lock_guard lock(mtx);
		inactiveLocations.insert({ instances[id]->id, instances[id]->GetLocations() });
		instances.erase(id);
	}

	void MediaObserverBase::Synchronize()
	{
		std::lock_guard lock(mtx);
		for (auto& location : removeQueue)
		{
			StopObserving(location);
		}
		removeQueue.clear();
		if (isReset)
		{
			for (auto& i : instances)
			{
				i.second->ResetLocations();
			}
			isReset = false;
		}
		for (auto& location : addQueue)
		{
			StartObserving(location);
		}
		addQueue.clear();
	}
}