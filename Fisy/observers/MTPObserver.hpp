#pragma once
#include "MediaObserverBase.hpp"
namespace Fisy::Observers
{
	//!todo: implement MTP observing
	class MTPObserver final :public MediaObserverBase
	{
	public:
		MTPObserver();
		~MTPObserver()override {};

	protected:
		class MTPInstance final :public Instance
		{
		public:
			MTPInstance();
			~MTPInstance()override {};
		};
	};
}