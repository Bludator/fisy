#include "Settings.hpp"
#include "parsingHelper.hpp"
#include <boost/filesystem.hpp>
#include "transfers/Drive.hpp"

namespace Fisy
{
	NLOHMANN_JSON_SERIALIZE_ENUM(MediaType, {
		{MediaType::Drive, "drive"},
		{MediaType::MTP, "MTP"},
		});

	void from_json(const nlohmann::json& j, Location& location)
	{
		j["mediaType"].get_to(location.mediaType);
		j["path"].get_to(location.path);

		//todo: quick fix
		boost::filesystem::path path(location.path);
		auto locId = j.find("mediaId");
		if (locId != j.end())
		{
			location.mediaId = locId.value().get<std::string>();
			if (path.has_root_path())
			{
				location.path = path.relative_path().string();
			}
		}
		else if (path.has_root_path())
		{
			location.mediaId = Transfers::Drive::RootToId(path.root_path().string());
			location.path = path.relative_path().string();
		}
		else
		{
			throw std::exception("No root");
		}
	}

	void from_json(const nlohmann::json& j, Settings& settings)
	{
		Default d(j);
		settings.confirm = d.Get("confirm", false);
		settings.move = d.Get("move", false);
		settings.once = d.Get("once", false);
		settings.oneDirection = d.Get("oneDirection", false);
		settings.delayMs = d.Get("delayMs", 5000);
		j.at("locations").get_to(settings.locations);

		for (auto& location : settings.locations)
		{
			location->settings = &settings;
		}
	}
	std::ostream& operator<<(std::ostream& outs, const Location& location)
	{
		std::string TypesEnumMap[] =
		{
			"Drive",
			"MTP",
		};
		outs << TypesEnumMap[(int)location.mediaType] << " with id (" << location.mediaId << ") on path: `" << location.path << "`";
		return outs;
	}
}