#pragma once
#include <chrono>
#include <mutex>
#include <vector>
#include <atomic>
#include "IStage.hpp"

namespace Fisy::Pipeline
{
	/// <summary>
	/// Provides delay feature and de-duplicate, simplify requests if possible
	/// </summary>
	class Optimize :public SimpleStage
	{
	public:
		Optimize();
		Optimize(std::chrono::milliseconds overrideDelay);
		~Optimize();
		virtual void Proceed(Data data) override;
	private:
		using clock = std::chrono::steady_clock;

		std::vector<Data> newData;
		std::vector<std::pair<clock::time_point, Data>> waiting;
		static std::vector<Data> updating;

		static std::mutex updatingMutex;
		std::mutex mtx;
		std::thread wt;
		//threadpool?
		std::atomic<bool> terminate = false;
		clock::time_point lastRun;
		void Run();
		void Add(Data incoming);
		std::optional<std::chrono::milliseconds> overrideDelay;
	};
}