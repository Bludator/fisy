#include "Optimize.hpp"
#include <thread>
#include <mutex>
namespace Fisy::Pipeline
{
	std::vector<Data> Optimize::updating;
	std::mutex Optimize::updatingMutex;

	Optimize::Optimize() :
		SimpleStage(),
		lastRun()
	{
		wt = std::move(std::thread(&Optimize::Run, this));
	}

	Optimize::Optimize(std::chrono::milliseconds overrideDelay) :Optimize()
	{
		this->overrideDelay = overrideDelay;
	}

	Optimize::~Optimize()
	{
		//inelegant?
		terminate = true;
		wt.join();
	}

	void Optimize::Proceed(Data data)
	{
		std::lock_guard lock(mtx);
		newData.push_back(std::move(data));
	}

	void Optimize::Run()
	{
		while (!terminate)
		{
			lastRun = clock::now();
			std::unique_lock lock(mtx);
			for (auto&& incoming : newData)
			{
				Add(std::move(incoming));
			}
			newData.clear();
			lock.unlock();

			auto it = waiting.begin();
			for (; it != waiting.end(); it++)
			{
				if (it->first > clock::now())
				{
					break;
				}
				//?todo: run ?in thread?
				std::unique_lock updatingLock(updatingMutex);
				updating.push_back(Data{ .from = std::make_unique<Location>(*it->second.from), .to = it->second.to ? std::make_unique<Location>(*it->second.to) : std::unique_ptr<Location>(nullptr), .trigger = it->second.trigger, .move = it->second.move });
				auto x = &*updating.back().from;
				updatingLock.unlock();
				this->nextStage->Proceed(std::move(it->second));
				updatingLock.lock();
				updating.erase(std::remove_if(updating.begin(), updating.end(), [&x](Data& y) {return x == &*y.from; }));
				updatingLock.unlock();
			}
			waiting.erase(waiting.begin(), it);
			std::this_thread::sleep_until(lastRun + std::chrono::seconds(1));
		}
	}

	void Optimize::Add(Data incoming)
	{
		std::unique_lock updatingLock(updatingMutex);
		for (auto& data : updating)
		{
			// remove false positive
			if (data.to && *incoming.from == *data.to) return;
			// try deduplicate
			if (incoming.to && data.to && *incoming.to == *data.to) {}// cancel
		}
		updatingLock.unlock();

		clock::time_point myTime = clock::now();
		if (overrideDelay)
		{
			myTime += overrideDelay.value();
		}
		else
		{
			if (incoming.trigger->settings != nullptr)
			{
				myTime += std::chrono::milliseconds(incoming.trigger->settings->delayMs);
			}
		}

		for (auto x = 0; x < waiting.size(); x++)
		{
			Data& data = waiting[x].second;
			// deduplicate ~ last `to` wins
			if (incoming.to && data.to && *incoming.to == *data.to)
			{
				waiting.erase(waiting.begin() + x);
				continue;
			}
			// updated source is waiting target -> remove waiting
			else if (data.to && *incoming.from == *data.to)
			{
				waiting.erase(waiting.begin() + x);
				continue;
			}
			// incoming target is waiting  source ~ swap
			else if (incoming.to && *incoming.to == *data.from && !data.from->settings->once) {}//?todo: wait
		}

		auto lastBefore = std::find_if(waiting.begin(), waiting.end(), [myTime](auto& x) {return x.first < myTime; });
		if (lastBefore != waiting.end())
		{
			lastBefore++;
		}
		waiting.emplace(lastBefore, myTime, std::move(incoming));
	}
}