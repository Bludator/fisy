#pragma once
#include "IStage.hpp"
#include "../transfers/Transfer.hpp"
namespace Fisy::Pipeline
{
	/// <summary>
	/// Find destinations, slightly change meaning of data in `Data`:
	/// Incoming data: File has been moved `from` `to`
	/// Outgoing data: Move file `from` `to`
	/// </summary>
	class FindDestionations :public SimpleStage
	{
	public:
		FindDestionations(Transfers::Transfer& transfer) :transfer(transfer) {}
		virtual void Proceed(Data data) override;;
	private:
		Transfers::Transfer& transfer;
		static std::string changePathPrefix(std::string path, std::string originalPrefix, std::string newPrefix);
		void ForAllLocations(Data& data, std::function<std::pair<std::unique_ptr<Location>, std::unique_ptr<Location>>(Location&)> getFromTo);
	};
}