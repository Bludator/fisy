#include "FindDestionations.hpp"
#include <functional>

namespace Fisy::Pipeline
{
	void FindDestionations::ForAllLocations(Data& data, std::function<std::pair<std::unique_ptr<Location>, std::unique_ptr<Location>>(Location&)> getFromTo)
	{
		if (data.trigger->settings == nullptr) //then it is probably just some notification e.g. config change
		{
			nextStage->Proceed(std::move(data));
			return;
		}
		for (auto&& loc : data.trigger->settings->locations)
		{
			if (&*loc == data.trigger) continue;

			auto [from, to] = getFromTo(*loc);

			auto fromMetadata = transfer.GetMetadata(&*from);
			auto toMetadata = transfer.GetMetadata(&*to);
			if (!fromMetadata.reachable || !toMetadata.reachable) continue;

			if (fromMetadata.lastWrite > toMetadata.lastWrite)
			{
				nextStage->Proceed(
					{
						.from = std::move(from),
						.to = std::move(to),
						.trigger = data.trigger,
						.move = data.move
					});
			}
		}
	}

	std::string FindDestionations::changePathPrefix(std::string path, std::string originalPrefix, std::string newPrefix)
	{
		assert(path.length() >= originalPrefix.length());
		return newPrefix + path.substr(originalPrefix.length());
	}

	//filter base on last write time!! otherwise we'll have to filter out changes by us - our transfers
	void FindDestionations::Proceed(Data data)
	{
		if (data.to == nullptr)//remove
		{
			assert(data.from);
			ForAllLocations(data, [&data](Location& loc)->std::pair<std::unique_ptr<Location>, std::unique_ptr<Location>>
			{
				auto from = std::make_unique<Location>(loc);
				from->path = changePathPrefix(data.from->path, data.trigger->path, loc.path);
				return { std::move(from), nullptr };
			});
		}
		else if (data.from == nullptr)// write / move or copy from somewhere ~ null from
		{
			ForAllLocations(data, [&data](Location& loc)->std::pair<std::unique_ptr<Location>, std::unique_ptr<Location>>
			{
				auto to = std::make_unique<Location>(loc);
				to->path = changePathPrefix(data.to->path, data.trigger->path, loc.path);
				return { std::make_unique<Location>(*data.to), std::move(to) };
			});
		}

		else// move/copy/rename ~ known from and to
		{
			ForAllLocations(data, [&data](Location& loc)->std::pair<std::unique_ptr<Location>, std::unique_ptr<Location>>
			{
				auto from = std::make_unique<Location>(loc);
				auto to = std::make_unique<Location>(loc);
				from->path = changePathPrefix(data.from->path, data.trigger->path, loc.path);;
				to->path = changePathPrefix(data.to->path, data.trigger->path, loc.path);;
				return { std::move(from), std::move(to) };
			});
		}
	}
}