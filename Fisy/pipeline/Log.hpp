#pragma once
#include <iostream>
#include "IStage.hpp"
#include <mutex>
#include <boost/nowide/iostream.hpp>

namespace Fisy::Pipeline
{
	/// <summary>
	/// Logs `Data`
	/// </summary>
	class Log :public SimpleStage
	{
	public:
		Log(std::string name) :logPointName(name), output(boost::nowide::cout) {}
		Log(std::string name, std::ostream& output) :logPointName(name), output(output) {}

		virtual void Proceed(Data data) override
		{
			namespace nowide = boost::nowide;
			std::unique_lock lock(mtx);
			output << "Logging at: " << logPointName << std::endl;
			if (data.from) output << '\t' << "From: " << *data.from << std::endl;
			if (data.to) output << '\t' << "To: " << *data.to << std::endl;
			assert(data.trigger != nullptr);
			assert(data.trigger->settings != nullptr);
			output << '\t' << "Triggered by: " << *data.trigger << std::endl;
			output << '\t' << "IsMoving: " << data.move << std::endl;
			output << std::endl;
			lock.unlock();
			if (nextStage)
			{
				nextStage->Proceed(std::move(data));
			}
		}
	private:
		inline static std::mutex mtx;
		std::string logPointName;
		std::ostream& output;
	};
}