#pragma once
#include "IStage.hpp"
namespace Fisy::Pipeline
{
	/// <summary>
	/// Provides "one direction" feature
	/// </summary>
	class OneDirection :public SimpleStage
	{
	public:
		/// <summary>
		/// If the sync is in "oneDirection" mode filter out all requests where first Location is not source
		/// </summary>
		/// <param name="data"></param>
		virtual void Proceed(Data data) override
		{
			if (!data.trigger->settings->oneDirection ||
				&*data.trigger == &*data.from->settings->locations[0])
			{
				nextStage->Proceed(std::move(data));
			}
		};
	};
}