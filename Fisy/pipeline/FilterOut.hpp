#pragma once
#include <unordered_set>
#include <functional>
#include "IStage.hpp"

namespace Fisy::Pipeline
{
	/// <summary>
	/// Filter out provided locations
	/// </summary>
	class FilterOut :public SimpleStage
	{
	public:
		FilterOut(const std::unordered_set<const Location*>& filteredPaths, std::function<void(Data data)> onChange)
			:filteredPaths(filteredPaths),
			onChange(onChange)
		{}
		/// <summary>
		/// Filter out provided locations and call onChange with the data, other wise do nothing - proceed next stage
		/// </summary>
		/// <param name="data"></param>
		virtual void Proceed(Data data) override
		{
			if (filteredPaths.find(data.trigger) != filteredPaths.end())
			{
				onChange(std::move(data));
				return;
			}
			nextStage->Proceed(std::move(data));
		};

	private:
		const std::unordered_set<const Location*>& filteredPaths;
		std::function<void(Data data)> onChange;
	};
}