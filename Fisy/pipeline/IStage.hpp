#pragma once
#include <memory>
#include "../Settings.hpp"

/// <summary>
/// Contains <see cref="IStage">Stages</see> of Pipeline
/// </summary>
namespace Fisy::Pipeline
{
	/// <summary>
	/// Data sent in pipeline
	/// </summary>
	struct Data {
		std::unique_ptr<Location> from;
		std::unique_ptr<Location> to;
		/// <summary>
		/// What location triggered this action.
		/// </summary>
		const Location* trigger;
		/// <summary>
		/// Is it copy (false) ore move (true)
		/// </summary>
		bool move;
	};

	/// <summary>
	/// Stage in Pipeline
	/// </summary>
	struct IStage
	{
		/// <summary>
		/// Process request and based on result send (or not) to next stage
		/// </summary>
		/// <param name="data"></param>
		virtual void Proceed(Data data) = 0;

		/// <summary>
		/// Sets next stage
		/// </summary>
		/// <param name="nextStage"></param>
		/// <returns> pointer to provided next stage</returns>
		virtual IStage* Next(std::shared_ptr<IStage> nextStage) = 0;
		virtual ~IStage() = 0 {};
	};

	/// <summary>
	/// Basic implementation of <see cref="IStage"/>
	/// </summary>
	class SimpleStage :public virtual IStage {
	public:
		IStage* Next(std::shared_ptr<IStage> nextStage)override
		{
			this->nextStage = std::move(nextStage);
			return &*this->nextStage;
		};

	protected:
		std::shared_ptr<IStage> nextStage;
	};
}