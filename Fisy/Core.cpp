#include "Core.hpp"

#include <vector>
#include <stack>
#include <fstream>
#include <algorithm>
#include <nlohmann/json.hpp>
#include <memory>

#include "parsingHelper.hpp"

#include "pipeline/FilterOut.hpp"
#include "pipeline/Optimize.hpp"
#include "pipeline/FindDestionations.hpp"
#include "pipeline/Log.hpp"
#include "pipeline/OneDirection.hpp"

#include "observers/DriveObserver.hpp"
#include "transfers/Drive.hpp"

#include <boost/nowide/filesystem.hpp>
#include <boost/filesystem.hpp>

using namespace std;
namespace fs = boost::filesystem;
namespace Fisy
{
	Init::Init() :FilesToSync(), observe()
	{
		boost::nowide::nowide_filesystem();

		//?todo: use initialization_list somehow?
		std::vector<unique_ptr<Fisy::Observers::IObservableMedia>> observersList;
		observersList.emplace_back(make_unique<Observers::DriveObserver>());
		observe = make_unique<Observers::Observe>(move(observersList));

		std::vector<unique_ptr<Fisy::Transfers::ITransferable>> transfersList;
		transfersList.emplace_back(make_unique<Transfers::DriveTransfer>());
		transfer = make_shared<Transfers::Transfer>(move(transfersList));

		BuildPipeline();
	}

	void Init::BuildPipeline()
	{
		using namespace Pipeline;
		auto filter = make_shared<OneDirection>();
		observe
			->NextOnChange(make_shared<Log>("On change"))
			//->Next(make_shared<FilterOut>(configLocations, [this](Pipeline::Data data)->void {OnConfigChanged(std::move(data)); }))
			->Next(make_shared<FindDestionations>(*transfer))
			->Next(make_shared<Optimize>())
			->Next(filter)
			;

		observe
			->NextOnConnect(make_shared<Log>("On connect"))
			->Next(make_shared<FindDestionations>(*transfer))
			->Next(make_shared<Optimize>(std::chrono::milliseconds(2000)))
			->Next(filter)
			;

		filter
			//->Next(make_shared<Once>()) //todo: make like FilterOut and then remove
			->Next(transfer)
			->Next(make_shared<Log>("Transfered"))
			;
	}

	void Init::OnConfigChanged(Pipeline::Data data)
	{
		//?todo: change just changed
		ResetObservers();
		std::this_thread::sleep_for(std::chrono::seconds(2));
		LoadConfig();
		StartObserving();
	}

	void Init::LoadConfig()
	{
		using JSON = nlohmann::json;
		FilesToSync.clear();
		configLocations.clear();

		stack<string> configPaths;
		configPaths.push(DEFAULT_CONFIG_PATH);

		while (!configPaths.empty())
		{
			auto location = fs::absolute(fs::path(configPaths.top()));
			configPaths.pop();
			if (!fs::exists(location))
			{
				continue;
			}

			auto dummy = std::make_unique<Settings>();
			dummy->locations.push_back(std::move(Transfers::Drive::GetLocation(location)));
			configLocations.insert(&*dummy->locations[0]);
			dummy->locations[0]->settings = &*dummy;
			FilesToSync.push_back(std::move(dummy));

			std::fstream config(location.string());
			JSON json;
			Default def(json);
			config >> json;

			vector<string> newLocations = std::move(def.Get("configLocations", vector<string>()));
			vector<unique_ptr<Settings>> newSettings = std::move(def.Get("inSync", move(vector<unique_ptr<Settings>>())));

			//?todo: do in parsing
			for (auto& setting : newSettings)
			{
				for (auto& loc : setting->locations)
				{
					loc->settings = &*setting;
				}
			}

			for_each(newLocations.cbegin(), newLocations.cend(), [&configPaths](string loc) {configPaths.push(loc); });
			FilesToSync.insert(FilesToSync.end(), std::make_move_iterator(newSettings.begin()), std::make_move_iterator(newSettings.end()));
		}
	}
	void Init::StartObserving()
	{
		for (auto& s : FilesToSync)
		{
			observe->Add(&*s);
		}
	}
}