#pragma once
#include <unordered_set>
#include <vector>
#include <string>
#include <memory>

#include "Settings.hpp"

#include "observers/Observe.hpp"
#include "transfers/Transfer.hpp"

namespace Fisy
{
	/// <summary>
	/// App entrypoint
	/// </summary>
	class Init
	{
	public:
		const std::string DEFAULT_CONFIG_PATH = "config.json";

		Init();

		/// <summary>
		/// Load configurations recursively from `DEFAULT_CONFIG_PATH`
		/// </summary>
		void LoadConfig();

		/// <summary>
		/// Register locations to observers
		/// </summary>
		void StartObserving();

		/// <summary>
		/// Reset all observers ~ stop observing, remove locations from them
		/// </summary>
		void ResetObservers()
		{
			observe->Reset();
		};

	private:
		/// <summary>
		/// Settings to sync
		/// </summary>
		std::vector<std::unique_ptr<Settings>> FilesToSync;

		/// <summary>
		/// Builds Pipeline
		/// </summary>
		void BuildPipeline();

		std::unordered_set<const Location*> configLocations;
		void OnConfigChanged(Pipeline::Data data);

		std::unique_ptr<Observers::Observe> observe;
		std::shared_ptr<Transfers::Transfer> transfer;
	};
}