#include <thread>
#include "Core.hpp"
#include "UserException.hpp"

using namespace std;

int main()
{
	Fisy::Init app;
	try
	{
		app.LoadConfig();
		app.StartObserving();
		std::this_thread::sleep_until(std::chrono::time_point<std::chrono::system_clock>::max());
	}
	catch (const Fisy::UserException& e)
	{
		std::cout << e.what();
	}

	return 0;
}