#pragma once
#include <vector>
#include <string>
#include <memory>
#include <functional>

#include <nlohmann/json.hpp>
#include <boost/container_hash/hash.hpp>

namespace Fisy
{
	struct Settings;

	enum class MediaType :char
	{
		Drive,
		MTP,
	};

	/// <summary>
	/// Location of file or folder
	/// </summary>
	struct Location
	{
		/// <summary>
		/// The media type. e.g. drive, MTP, PTP, Network something etc.
		/// </summary>
		MediaType mediaType;

		/// <summary>
		/// The media identifier
		/// </summary>
		/// <seealso cref="https://en.wikipedia.org/wiki/Volume_serial_number">
		std::string mediaId;
		/// <summary>
		/// The path segment of location
		/// </summary>
		std::string path;
		Settings* settings;

		bool operator==(Location& rhs)
		{
			return
				mediaType == rhs.mediaType &&
				mediaId == rhs.mediaId &&
				path == rhs.path;
		}
	};
	/// <summary>
	/// For logging purpose, it is definitely not for conversion
	/// </summary>
	/// <param name="outs"></param>
	/// <returns></returns>
	std::ostream& operator<<(std::ostream& outs, const Location& location);

	/// <summary>
	/// Represents set of <see cref="Location"/>s to synchronize with settings
	/// </summary>
	/// It is parsed object from `inSync` in `config.json`
	struct Settings
	{
		/// <summary>
		/// Specifies if files will be moved instead of copied. Move = copy and delete
		/// </summary>
		bool move;

		/// <summary>
		/// Specifies if syncing occurs just in one direction.
		/// If set to true location will be synced from 0th position to the others in <see cref="this->locations"/>
		/// </summary>
		bool oneDirection;

		/// <summary>
		/// After one sync this setting will be removed
		/// </summary>
		bool once;

		/// <summary>
		/// If true, user will be prompted before syncing
		/// </summary>
		bool confirm;

		/// <summary>
		/// The delay of syncing in ms
		/// </summary>
		int delayMs;

		/// <summary>
		/// The <see cref="Location"/>s to sync.
		/// </summary>
		std::vector<std::unique_ptr<Location>> locations;
	};

	// void to_json(nlohmann::json& j, const Settings& settings);
	void from_json(const nlohmann::json& j, Settings& settings);

	// void to_json(nlohmann::json& j, const Location& location);
	void from_json(const nlohmann::json& j, Location& location);

	template <class T>
	class Hash;

	template<>
	struct Hash<Location>
	{
		std::size_t operator()(const Location& l) const
		{
			std::size_t res = 0;
			boost::hash_combine(res, l.mediaType);
			boost::hash_combine(res, l.mediaId);
			boost::hash_combine(res, l.path);
			boost::hash_combine(res, l.settings);
			return res;
		}
	};

	inline bool operator==(Location const& left, Location const& right)
	{
		return (Hash<Location>()(left) == Hash<Location>()(right));
	}
}