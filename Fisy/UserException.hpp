#pragma once
#include <stdexcept>
namespace Fisy {
	/// <summary>
	/// Exception caused by invalid user provided data, usually in config file
	/// </summary>
	class UserException : public std::runtime_error
	{
	public:
		UserException(std::string msg) :std::runtime_error(msg)
		{
		}
	};
}