#pragma once
#include <string>
#include <string>
#include <nlohmann/json.hpp>
#include <memory>

namespace Fisy
{
	/// <summary>
	/// Provides methods to parse json with default values
	/// </summary>
	class Default
	{
	public:
		Default(const nlohmann::json& j) :j(j) {};

		template <typename T>
		static T Get1(const nlohmann::json& j, std::string at, T defaultVal)
		{
			auto input = j.find(at);
			return (input != j.end()) ? input.value().get<T>() : std::move(defaultVal);
		}

		template <typename T>
		/// <summary>
		/// Try parse value at <param name="at">at</param> and if unsuccessful return default
		/// </summary>
		/// <param name="at"></param>
		/// <param name="defaultVal"></param>
		/// <returns>parsed value or default value</returns>
		T Get(std::string at, T defaultVal)
		{
			return  Get1(j, at, std::move(defaultVal));
		}

	private:
		const nlohmann::json& j;
	};
}
namespace nlohmann {
	template<typename T>
	struct adl_serializer<std::unique_ptr<T>>
	{
		static void to_json(json& j, const std::unique_ptr<T>& value)
		{
			if (!value)
			{
				j = nullptr;
			}
			else
			{
				j = *value;
			}
		}

		static void from_json(json const& j, std::unique_ptr<T>& value)
		{
			if (j.is_null())
			{
				value.reset();
			}
			else
			{
				value = std::make_unique<T>();
				*value = j.get<T>();
			}
		}
	};
}